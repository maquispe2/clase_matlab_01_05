clc;
clear all;
%Cargar la memoria
%Importar la memoria compartida
loadlibrary('smClient64.dll','./smClient.h')
%Abrir la memoria para movimiento
calllib('smClient64','openMemory','Movimiento',2)
%Abrir la memoria para movimiento
calllib('smClient64','openMemory','Posiciones',2)
%Escribir o Leer memoria
movi_x = 0;
while true
    %escribimos la posicion al cubo
    calllib('smClient64','setFloat','Movimiento',0,movi_x);
    movi_x = movi_x + 0.1;
    %Leer las posiciones del cubo
    x = calllib('smClient64','getFloat','Posiciones',0)
    y = calllib('smClient64','getFloat','Posiciones',1)
    z = calllib('smClient64','getFloat','Posiciones',2)
end

%Liberar memoria
