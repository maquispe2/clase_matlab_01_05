clc;
clear all;
%Importar la memoria compartida
loadlibrary('smClient64.dll','./smClient.h')
%Creamos la memoria compartida(En este caso lo isimos desde el panel)
%Abrir la memoria compartida 
calllib('smClient64','openMemory','NumerosEnteros',1)
%Escribir en la memoria compartida
x = 10;
y = 5;
while true
    calllib('smClient64','setInt','NumerosEnteros',0,x);
    calllib('smClient64','setInt','NumerosEnteros',1,y);
    calllib('smClient64','setInt','NumerosEnteros',2,y);
    calllib('smClient64','setInt','NumerosEnteros',3,y);
    x = x+1;
    pause(1)
end

%Leer la memoria compartida
retInt = calllib('smClient64','getInt','NumerosEnteros',1)
%Liberar la memoria compartida
calllib('smClient64','freeViews')
unloadlibrary smClient64