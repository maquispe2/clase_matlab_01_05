clc;
clear all;
%Importar la memoria compartida
loadlibrary('smClient64.dll','./smClient.h')
%Creamos la memoria compartida(En este caso lo isimos desde el panel)
%Abrir la memoria compartida 
calllib('smClient64','openMemory','Sp',2)
calllib('smClient64','openMemory','Controlador',2)
%Escribimos o leemos la memoria
while true
    %Obtenemos el SetPoint desde Unity
    sp = calllib('smClient64','getFloat','Sp',0);
    %Aplicamos el algoritmo de control
    [Valor_Control] = CONTROLADOR_PID(sp);
    %Enviamo el valor de control a Unity
    calllib('smClient64','setFloat','Controlador',0,Valor_Control);
end
%Liberamos la memoria
calllib('smClient64','freeViews')
unloadlibrary smClient64

%% CONTROLADOR
function [Valor_Control] = CONTROLADOR_PID(sp);
    Valor_Control = sp * 0.2;
end